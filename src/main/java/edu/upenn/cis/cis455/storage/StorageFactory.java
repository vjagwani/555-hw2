package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.nio.file.Path;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

public class StorageFactory {
    
    public static StorageInterface getDatabaseInstance(String directory) {
	  // TODO: factory object, instantiate your storage server
	   Environment db = null;
	   EntityStore store = null;
	   try {
	       EnvironmentConfig config = new EnvironmentConfig();
	       StoreConfig storeConfig = new StoreConfig();
	       config.setAllowCreate(true);
	       storeConfig.setAllowCreate(true);
	       storeConfig.setDeferredWrite(true);
	       File dir = new File(directory);
	       if(dir.exists()){
	           db = new Environment(dir, config);
	       } else {
	           dir.mkdirs();
	           db = new Environment(dir, config);
	       }
	       store = new EntityStore(db, "EntityStore", storeConfig);
	   } catch (DatabaseException e) {
	       System.err.println(e.toString());
	   }
	   return new ImplementStorage(store, db);
    }
    
}
