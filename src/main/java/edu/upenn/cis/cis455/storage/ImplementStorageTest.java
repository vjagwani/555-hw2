package edu.upenn.cis.cis455.storage;

import org.junit.Test;
import org.junit.Assert;

import edu.upenn.cis.cis455.crawler.info.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class ImplementStorageTest extends TestCase {
    
    @Test
    public void testAddUser() {
        
        StorageInterface db = StorageFactory.getDatabaseInstance("testdb");
        Assert.assertTrue(db.addUser("test", "pass", "first", "last") > 0);
        Assert.assertTrue(db.addUser("test", "pass", "first", "last") == -1);
    }
    
    @Test
    public void testAddDocument() {
        StorageInterface db = StorageFactory.getDatabaseInstance("testdb");
        Assert.assertTrue(db.addDocument("website.com", "jajaja", "html") == 0);
        Assert.assertTrue(db.addDocument("website.com", "jajaja", "html") == -1);
    }
}
