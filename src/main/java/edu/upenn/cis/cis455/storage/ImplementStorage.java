package edu.upenn.cis.cis455.storage;

import java.util.UUID;

import com.sleepycat.je.Environment;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

import org.apache.commons.codec.digest.DigestUtils;

import edu.upenn.cis.cis455.model.Document;
import edu.upenn.cis.cis455.model.User;

public class ImplementStorage implements StorageInterface{
    
    public EntityStore store;
    public Environment db;
    
    public ImplementStorage (EntityStore store, Environment db) {
        this.store = store;
        this.db = db;
    }

	@Override
	public int getCorpusSize() {
	    PrimaryIndex<String, Document> pi = store.getPrimaryIndex(String.class, Document.class);
		return (int) pi.count();
	}

	@Override
	public int addDocument(String url, String documentContents, String type) {
	    PrimaryIndex<String, Document> pi = store.getPrimaryIndex(String.class, Document.class);
	    String md5 = DigestUtils.md5Hex(documentContents);
	    if(pi.contains(url)) {
	        Document d = pi.get(url);
	        if (d.getDocMd5().equals(md5)) {
	            return -1;
	        }
	    }
	    pi.put(new Document(documentContents, url, md5, type));
		return 0;
	}

	@Override
	public int getLexiconSize() {
		return 0;
	}

	@Override
	public int addOrGetKeywordId(String keyword) {
		return 0;
	}

	@Override
	public int addUser(String username, String password, String firstname, String lastname) {
	    PrimaryIndex<String, User> pi = store.getPrimaryIndex(String.class, User.class);
	    if(pi.contains(username)) {
	        return -1;
	    }
	    int userId = (username + password + firstname + lastname).hashCode();
	    boolean submitted = false;
	    int i = 0;
	    while(!submitted){
	        userId = (username + password + firstname + lastname).hashCode() + i;
	        submitted = pi.putNoOverwrite(new User(userId, username, password, firstname, lastname));
	        i ++;
	    }
		return userId;
	}

	@Override
	public boolean getSessionForUser(String username, String password) {
	    PrimaryIndex<String, User> pi = store.getPrimaryIndex(String.class, User.class);
	    if(pi.contains(username)) {
	        User temp = pi.get(username);
	        if(temp.getPassword().equals(password)) {
	            return true;
	        }
	    }
		return false;
	}

	@Override
	public String getDocument(String url) {
	    PrimaryIndex<String, Document> pi = store.getPrimaryIndex(String.class, Document.class);
	    if(pi.contains(url)) {
	        Document d = pi.get(url);
	        return d.getDocBody();
	    }
	    return null;
	}

	@Override
	public void close() {
	    db.sync();
	    store.sync();	
	}

	@Override
	public String getFirstname(String username) {
	    PrimaryIndex<String, User> pi = store.getPrimaryIndex(String.class, User.class);
	    if(pi.contains(username)) {
	        User temp = pi.get(username);
	        return temp.getFirstname();
	    }
		return null;
	}

	@Override
	public String getLastname(String username) {
		PrimaryIndex<String, User> pi = store.getPrimaryIndex(String.class, User.class);
	    if(pi.contains(username)) {
	        User temp = pi.get(username);
	        return temp.getLastname();
	    }
		return null;
	}
}
