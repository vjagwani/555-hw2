package edu.upenn.cis.cis455.model;

import java.io.Serializable;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class Document implements Serializable {
    @PrimaryKey
    String docUrl;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String docMd5;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String docBody;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String type;
    
    public Document() {
        this.docBody = "";
        this.docUrl = "";
        this.docMd5 = "";
        this.type = "";
    }
    
    
    public Document(String docBody, String docUrl, String docMd5, String type) {
        this.docBody = docBody;
        this.docUrl = docUrl;
        this.docMd5 = docMd5;
        this.type = type;
    }
    
    public String getDocUrl() {
        return docUrl;
    }
    
    public String getDocBody() {
        return docBody;
    }
    
    public String getDocMd5() {
        return docMd5;
    }
    public String type() {
        return type;
    }
}
