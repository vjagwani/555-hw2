package edu.upenn.cis.cis455.model;

import java.io.Serializable;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class User implements Serializable {
    @SecondaryKey(relate=Relationship.ONE_TO_ONE)
    Integer userId;
    @PrimaryKey
    String userName;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String password;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String firstname;
    @SecondaryKey(relate=Relationship.MANY_TO_ONE)
    String lastname;
    
    public User() {
        this.userId = -1;
        this.userName = "";
        this.password = "";
        this.firstname = "";
        this.lastname = "";
    }
    
    
    public User(Integer userId, String userName, String password, String firstname, String lastname) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
    }
    
    public Integer getUserId() {
        return userId;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getFirstname() {
        return firstname;
    }
    
    public String getLastname() {
        return lastname;
    }
}
