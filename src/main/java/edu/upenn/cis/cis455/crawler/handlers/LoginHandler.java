package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;

import org.apache.commons.codec.digest.DigestUtils;
import java.util.Date;

import edu.upenn.cis.cis455.storage.StorageInterface;

public class LoginHandler implements Route {
    StorageInterface db;
    
    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
        
        System.err.println("Login request for " + user + " and " + pass);
        
        String passHash = DigestUtils.sha256Hex(pass);
        // log userin
        if (db.getSessionForUser(user, passHash)) {
            System.err.println("Logged in!");
            Session session = req.session(true);
            session.attribute("username", user);
            session.attribute("password", passHash);
            session.attribute("firstname", db.getFirstname(user));
            session.attribute("lastname", db.getLastname(user));
            session.attribute("lastAccessed", Long.toString(new Date().getTime()));
            resp.redirect("/index.html");
        } else {
            System.err.println("Invalid credentials");
            resp.redirect("/login-form.html");
        }
        return "";
    }
}
