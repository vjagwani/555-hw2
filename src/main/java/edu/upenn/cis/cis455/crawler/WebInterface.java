package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static spark.Spark.*;

import edu.upenn.cis.cis455.crawler.handlers.IndexHandler;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.staticfiles.StaticFilesConfiguration;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.LookupHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegisterHandler;

public class WebInterface {
    public static void main(String args[]) {
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }
        
        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        port(8080);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);
        // TODO:  add /register, /logout, /index.html, /, /lookup
        //post("/register", new RegistrationHandler(database));
        
         // If logged in filter
        before(new LoginFilter(database));
        
        // Index page
        get("/index.html", new IndexHandler());
        redirect.get("/","/index.html");
        
        if (args.length == 2) {
            StaticFilesConfiguration config = new StaticFilesConfiguration();
            config.configureExternal(args[1]);
            before((request, response) -> config.consume(request.raw(), response.raw()));
//            staticFiles.externalLocation(args[1]);
//            staticFileLocation(args[1]);
        }
        
        // post login request
        post("/login", new LoginHandler(database));
        
        // post register new user request
        post("/register", new RegisterHandler(database));
        
        // post logout request
        post("/logout", new LogoutHandler(database));
        
        // post logout request
        get("/lookup", new LookupHandler(database));
        
        // sync 
        after((request, response)-> database.close());
        awaitInitialization();
    }
}
