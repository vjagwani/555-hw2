package edu.upenn.cis.cis455.crawler;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.info.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.info.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.CrawlMaster;


public class Crawler implements CrawlMaster {
    static final int NUM_WORKERS = 10;
    public String startUrl;
    public StorageInterface db;
    public Integer maxDocSize,maxCount;
    public Integer indexedDocuments = 0;
    public Integer numWorkingThreads = 0;
    public static int exitedThreads = 0;
    
    // Thread pool
    public ArrayList<CrawlerWorker> pool = new ArrayList<CrawlerWorker>();

    // List of url to be worked on
    public LinkedList<String> urls = new LinkedList<String>();
    
    // List of url already worked on
    public HashSet<String> seenUrls = new HashSet<String>();
    
    
    // Map from hostnames to robotTxts
    public HashMap<String, RobotsTxtInfo> robotMap;
    
    // Map from hostnames to lastAccessedTime
    public HashMap<String, Long> accessMap = new HashMap<String, Long>();
    
    
    // These are Thread Workers
    public class CrawlerWorker extends Thread {
    	@Override
    	public void run() {
    	    //System.out.println("Online");
    	    Logger log = LogManager.getLogger(CrawlerWorker.class);
    	    org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.INFO);
    		while(!isDone()) {
    		    String workUrl = null;
    		    synchronized(urls) {
    		        if(!urls.isEmpty()) {
    		            setWorking(true);
        		        workUrl = urls.poll();
    		        } 
    		    }
    		 
    		    if (workUrl == null){
        		        try {
    						Thread.sleep(20);
    					} catch (InterruptedException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    		    } else {
    		        synchronized(seenUrls){
    		            if(seenUrls.contains(workUrl)) {
    		                workUrl = null;
    		                setWorking(false);
    		            } else {
    		                seenUrls.add(workUrl);
    		            }
		           }
    		    }
    		    if (workUrl != null) {
    		        URLInfo link = new URLInfo(workUrl);
    		        if(isOKtoCrawl(workUrl,link.getPortNo(),link.isSecure())) {
    		            // Sleep if defer crawl active
    		            if(deferCrawl(link.getHostName())) {
                            Long lastAccess = null;
                            synchronized(accessMap){
                                lastAccess = accessMap.get(link.getHostName());
                            }
                            if (lastAccess == null) {
                                continue;
                            } else {
                                long timeSince = new Date().getTime() - lastAccess;
                                Integer waitTime = 0;
                                HashMap<String, RobotsTxtInfo> tempRbtMap; 
                                synchronized(robotMap) {
                                    tempRbtMap = robotMap;
                                }
                                waitTime = tempRbtMap.get(link.getHostName()).getCrawlDelay("cis455crawler");
                                if(waitTime == null) {
                                        waitTime = tempRbtMap.get(link.getHostName()).getCrawlDelay("*");
                                }
                                if (waitTime != null){
                                    if((int)timeSince < waitTime * 1000) {
                                        synchronized(urls) {
                                            urls.add(workUrl);
                                        }
                                        synchronized(seenUrls) {
                                            seenUrls.remove(workUrl);
                                        }
                                        workUrl = null;
                                    }
                                }
                            }
    		            }
    		            //System.out.println("Got here");
    		            if(workUrl != null && isOKtoParse(link)) {
                            try {
                                HttpURLConnection connection = openConnection(link, workUrl, "HEAD");
                                if(connection != null)
                                    connection.connect();
                                // DO HEAD REQUEST FIRST IF HTTP 200 
                                if (connection != null && (connection.getResponseCode() == 200 || connection.getResponseCode() == 304)) {
    								BufferedReader br;
    								String line = null;
    								// if file type isn't html or xml, don't do
    								String cType = connection.getContentType();
    								if (cType != null && 
    								    !cType.contains("text/html") && 
    								    !cType.matches("(.*)/xml"))
    								    throw new IOException();
    								String type = "xml";
    								if(cType != null && cType.contains("html"))
    								    type = "html";
    								// if size of file is too big, don't do
    								if(connection.getContentLength() > maxDocSize * 1000000)
    								    throw new IOException();
    								//DO GET REQUEST NEXT, GIVEN HTTP 200
    								connection = openConnection(link, workUrl, "GET"); 
    								if(connection != null)
    								    connection.connect();
    								if (connection != null && (connection.getResponseCode() == 200 || connection.getResponseCode() == 304)) {
        								br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        								StringBuilder strBget = new StringBuilder();
        								while((line = br.readLine()) != null) {
        								    strBget.append(line);
        								}
                                        br.close();
                                        String html = strBget.toString();
                                        synchronized(indexedDocuments){
                                            if(indexedDocuments < maxCount && addDocument(workUrl, html, type) == 0) {
                                                incCount();
                                                log.info(workUrl + ": Downloading");
                                            } else if(indexedDocuments < maxCount) {
                                                log.info(workUrl + ": Not Modified");
                                            }
                                        }
                                       
                                        if(type.equals("html")) {
                                            // PARSE HTML DOCUMENT with jsoup
                                            Document doc = Jsoup.parse(html, link.securityString() + link.getHostName());
                                            Elements links = doc.select("a[href]");
                                            Collection<String> temp = new LinkedList<String>();
                                            for (Element exlink : links) {
                                                temp.add(exlink.attr("abs:href"));
                                            } synchronized(urls) {
                                                urls.addAll(temp);
                                            }
                                        }
    								}
                                }
							} catch (IOException e) {
								// TODO Auto-generated catch block
								;
							}
    		            }
    		        }
    		    setWorking(false);
    		    }
    		}
    		//System.out.println("Offline");
    		notifyThreadExited();
    	}
    }
    
    
    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
        this.startUrl = startUrl;
        this.db = db;
        this.maxDocSize = size;
        this.maxCount = count;
        this.pool = new ArrayList<CrawlerWorker>();
        this.urls = new LinkedList<String>();
        this.seenUrls = new HashSet<String>();
        this.robotMap = new HashMap<String, RobotsTxtInfo>();
        this.accessMap = new HashMap<String, Long>();
        
    }

    ///// TODO: you'll need to flesh all of this out.  You'll need to build a thread
    // pool of CrawlerWorkers etc. and to implement the functions below which are
    // stubs to compile
    
    // Method to open connections
    public HttpURLConnection openConnection(URLInfo link, String workUrl, String method) {
        HttpURLConnection connection;
        try {
            if(!link.isSecure()) {
                connection = (HttpURLConnection) new URL(workUrl).openConnection();
            } else {
                connection = (HttpsURLConnection) new URL(workUrl).openConnection();
            }
            connection.setRequestProperty("User-Agent","cis455crawler");
            connection.setRequestProperty("Host",link.getHostName());
            connection.setRequestMethod(method);
            synchronized (accessMap){
                accessMap.put(link.getHostName(), new Date().getTime());
            }
            return connection;
        } catch (Exception e) {
            return null;
        }
    }

    
    
    /**
     * Main thread
     */
    public void start() {
        urls.add(startUrl);
        for (int i = 0; i < NUM_WORKERS; i++) {
            pool.add(new CrawlerWorker());
            pool.get(i).start();
        }
    }
    
    /**
     * Returns true if it's permissible to access the site right now
     * eg due to robots, etc.
     */
    
    // Construct ROBOT TXT INFO if first time going to host
    public boolean isOKtoCrawl(String site, int port, boolean isSecure) {
            URLInfo link = new URLInfo(site);
            if (isSecure) link.setSecure(true);
            RobotsTxtInfo robotInfo = new RobotsTxtInfo();
            if(!robotMap.containsKey(link.getHostName())){
                HttpURLConnection connection = openConnection(link, link.securityString() + link.getHostName() + "/robots.txt", "GET");
                BufferedReader br;
				try {
				    if(connection != null)
				        connection.connect();
				    if(connection != null && (connection.getResponseCode() == 200 || connection.getResponseCode() == 304)){
				        br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    					String line = null;
    					String currentAgent = "";
    					while((line = br.readLine()) != null) {
    					    if(line.contains(":") && !line.startsWith("#")) {
    					        String [] directive = line.split(":");
    					        String key = directive[0].trim();
    					        String value = "";
    					        if(directive.length > 1) 
    					           value = directive[1].trim();
    					        if (key.equals("Allow")) {
    					            robotInfo.addAllowedLink(currentAgent,value);
    					        } else if(key.equals("Disallow")) {
    					            robotInfo.addDisallowedLink(currentAgent,value);
    					        } else if(key.equals("User-Agent")) {
    					            currentAgent = value;
    					            robotInfo.addUserAgent(value);
    					        } else if(key.equals("Sitemap")) {
    					            robotInfo.addSitemapLink(value);
    					        } else if(key.equals("Crawl-delay")) {
    					            robotInfo.addCrawlDelay(currentAgent,Integer.parseInt(value));
    					        }
    					    }
    					}
    					br.close();
    					synchronized(robotMap) {
    					    robotMap.put(link.getHostName(),robotInfo);
    					}
				    } else {
				        synchronized(robotMap){
				            robotMap.put(link.getHostName(), null);
				            
				        }
				    }
				} catch (IOException e) {
					e.printStackTrace();
			     }
            }
            synchronized(robotMap) {
                robotInfo = robotMap.get(link.getHostName());
                
            }
            if (robotInfo == null) {
                return true;
            }
            
            ArrayList<String> allowed;
            ArrayList<String> disallowed;
        
            if (robotInfo.containsUserAgent("cis455crawler")) {
                allowed = robotInfo.getAllowedLinks("cis455crawler");
                disallowed = robotInfo.getDisallowedLinks("cis455crawler");
            } else {
                allowed = robotInfo.getAllowedLinks("*");
                disallowed = robotInfo.getDisallowedLinks("*");
            }
            if (allowed == null && disallowed == null) {
                return true;
            }
            if (allowed == null && disallowed.contains("/")) {
                return false;
            }
            return true;
        }

    /**
     * Returns true if the crawl delay says we should wait
     */
    public boolean deferCrawl(String site) {
        RobotsTxtInfo robotInfo = new RobotsTxtInfo();
        synchronized(robotMap){
         robotInfo = robotMap.get(site);
        }
        if (robotInfo != null && (robotInfo.getCrawlDelay("cis455crawler") != null || robotInfo.getCrawlDelay("*") != null )) {
            return true; 
        }
        return false; 
    }
    
    /**
     * Returns true if it's permissible to fetch the content,
     */
    public boolean isOKtoParse(URLInfo url) {
        RobotsTxtInfo robotInfo = null;
        synchronized(robotMap) {
            robotInfo = robotMap.get(url.getHostName());
        }
        if (robotInfo == null) {
            return true;
        }
        ArrayList<String> allowed;
        ArrayList<String> disallowed;
        if (robotInfo.containsUserAgent("cis455crawler")) {
            allowed = robotInfo.getAllowedLinks("cis455crawler");
            disallowed = robotInfo.getDisallowedLinks("cis455crawler");
        } else {
            allowed = robotInfo.getAllowedLinks("*");
            disallowed = robotInfo.getDisallowedLinks("*");
        }
        if (allowed == null && disallowed == null) {
            return true;
        }
        int matchedLength = 0;
        String fp = url.getFilePath();
        if(fp == null) fp = "";
        fp = fp.trim();
        if (allowed != null) {
            for (String x : allowed) {
                x = x.trim();
                String y = x;
                if (x.contains("*")){
                    String[] temp = x.split("*");
                    y = String.join("(.*)", temp);
                }
                if(fp.matches(y) || fp.startsWith(x)) {
                    if (x.length() > matchedLength)
                        matchedLength = x.length();
                }
            }
        }
        
        if(disallowed != null) {
            for (String x : disallowed) {
                x = x.trim();
                String y = x;
                if (x.contains("*")){
                    String[] temp = x.split("*");
                    y = String.join("(.*)", temp);
                }
                if(fp.matches(y) || fp.startsWith(x)) {
                    if (x.length() > matchedLength)
                        return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Returns true if the document content looks worthy of indexing,
     * eg that it doesn't have a known signature
     */
    public int addDocument(String url, String content, String type) {
        return db.addDocument(url, content, type);
    }
    
    /**
     * We've indexed another document
     */
    public synchronized void incCount() {
            indexedDocuments ++;
    }
    
    /**
     * Workers can poll this to see if they should exit, ie the
     * crawl is done
     */
    
    public boolean isDone() {
        int indDoc, maxC;
        synchronized(indexedDocuments) {
            indDoc = indexedDocuments;
        }
        synchronized(maxCount){
            maxC = maxCount;
        }
        if(indDoc >= maxC){
            return true;
        }
        int numWorking;
        boolean empty;
        synchronized(numWorkingThreads) {
            numWorking = numWorkingThreads;
        }
        synchronized(urls) {
            empty = urls.isEmpty();
        }
        if(numWorking <= 0 && empty) {
            return true;
        }
        return false; 
    }
    
    public boolean threadsFinished() {
        int numWorking;
        boolean empty;
        synchronized(numWorkingThreads) {
            numWorking = numWorkingThreads;
        }
        synchronized(urls){
            empty = urls.isEmpty();
        }
        if(numWorking <= 0 && empty) {
            return true;
        }
        return false; 
    }
    
    /**
     * Workers should notify when they are processing an URL
     */
    public void setWorking(boolean working) {
        synchronized(numWorkingThreads){
            if(working) {
                numWorkingThreads ++;
            } else {
                numWorkingThreads --;
            }
            
        }
    }
    
    /**
     * Workers should call this when they exit, so the master
     * knows when it can shut down
     */
    public synchronized void notifyThreadExited() {
        exitedThreads ++;
    }
    
    /**
     * Main program:  init database, start crawler, wait
     * for it to notify that it is done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }
        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;
        
        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);
        
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();
        
        while (exitedThreads != NUM_WORKERS)
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        // TODO: final shutdown
        db.close();
        //System.out.println("Done crawling!");
        System.exit(0);
    }

}
