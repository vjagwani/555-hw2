package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;

import org.apache.commons.codec.digest.DigestUtils;

import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegisterHandler implements Route {
    StorageInterface db;
    
    public RegisterHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
        String firstname = req.queryParams("firstname");
        String lastname = req.queryParams("lastname");
        
        System.out.println("register request for " + user + " " + firstname + " " + lastname);
        
        // SHA256 password
        String passHash = DigestUtils.sha256Hex(pass);
        int id = db.addUser(user, passHash, firstname, lastname);
        // Case if username already registered
        if (id == -1) {
            System.err.println("Account already in use");
            resp.redirect("/register.html");
        } else {
            // new user added
            System.err.println("Added new user");
            resp.redirect("/login-form.html");
        } 
        return "";
    }
}
