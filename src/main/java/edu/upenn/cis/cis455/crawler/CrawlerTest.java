package edu.upenn.cis.cis455.crawler;

import org.junit.Test;
import org.junit.Assert;

import edu.upenn.cis.cis455.crawler.info.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class CrawlerTest extends TestCase {
    
    @Test
    public void testRobotDisallow() {
        String startUrl = "http://titan.dcs.bbk.ac.uk/~kikpef01/testpage.html";
        String envPath = "berkeleydb";
        Integer size = 10;
        Integer count = 3;
        
        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);
        
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();
        
        while (Crawler.exitedThreads != Crawler.NUM_WORKERS)
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        db.close();
        Assert.assertTrue(crawler.isOKtoParse(new URLInfo("http://titan.dcs.bbk.ac.uk/~kikpef01/testpage.html")));
    }
    
    @Test
    public void testCount() {
        String startUrl = "http://www.google.com";
        String envPath = "berkeleydb";
        Integer size = 0;
        Integer count = 1;
        
        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);
        
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();
        
        while (Crawler.exitedThreads != Crawler.NUM_WORKERS)
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        db.close();
        System.out.println("NUM DOCUMENTS : " + crawler.indexedDocuments );
        Assert.assertTrue(1 == crawler.indexedDocuments);
    }
}
