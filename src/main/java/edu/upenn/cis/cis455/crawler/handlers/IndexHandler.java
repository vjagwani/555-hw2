package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class IndexHandler implements Route {

	@Override
	public Object handle(Request request, Response response) throws Exception {
	    Session session = request.session(false);
	    if (session != null) {
	        String first = session.attribute("firstname");
	        String last = session.attribute("lastname");
	        response.body("<!DOCTYPE html><html><head><title>Welcome</title></head>" +
            "<body> <h1>Welcome " + first + " " + last + "</h1>" + "</body></html>");
            response.status(200);
            response.type("text/html");
	    }
		return null;
	}
}
