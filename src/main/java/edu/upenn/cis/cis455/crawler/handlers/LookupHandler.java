package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class LookupHandler implements Route {
    
    public StorageInterface db;
    
    public LookupHandler(StorageInterface db) {
        this.db = db;
    }

	@Override
	public Object handle(Request request, Response response) throws Exception {
	    Session session = request.session(false);
	    if (session != null) {
	        response.body(getResponse(request.queryParams("url")));
            response.status(200);
            response.type("text/html");
	    }
		return null;
	}
	
	public String getResponse(String url){
	    String temp = db.getDocument(url);
	    if(temp == null) {
	        return "<h1>Not Found</h1>";
	    } else {
	        return temp;
	    }
	    
	}
}
