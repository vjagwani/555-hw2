package edu.upenn.cis.cis455.crawler.handlers;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Filter;
import spark.Response;
import spark.Spark;


public class LoginFilter implements Filter {
    Logger logger = LogManager.getLogger(LoginFilter.class);
    public StorageInterface db = null;
    
    public LoginFilter(StorageInterface db) {
        this.db = db;
    }

    @Override
    public void handle(Request req, Response response) throws Exception {
        System.out.println(req.pathInfo());
        if (!req.pathInfo().equals("/login-form.html") &&
        !req.pathInfo().equals("/login") &&
        !req.pathInfo().equals("/register") &&
        !req.pathInfo().equals("/register.html") &&
        !req.pathInfo().equals("/logout")
        ) {
            logger.info("Request is NOT login/registration");
            // either no session, or session has been 'logged out' by deleting username attribute
            if (req.session(false) == null || !req.session().attributes().contains("username")) {
                // logger.info
                System.err.println("Not logged in - redirecting!");
                response.redirect("/login-form.html");
                Spark.halt();
            } else {
                Long lastAccess = Long.parseLong(req.session().attribute("lastAccessed"));
                if (new Date().getTime() - lastAccess > (long)(1000 * 60 * 5)) {
                    System.err.println("Session timed out");
                    response.redirect("/logout");
                    Spark.halt();
                }
                req.session().attribute("lastAccessed", Long.toString(new Date().getTime()));
                System.err.println("Logged in!");
                req.attribute("user", req.session().attribute("username"));
            }
        } else {
            //            logger.info
            System.err.println("Request is LOGIN FORM or REGISTER");
        }
        
    }
}
